extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

enum MENU{
	resume,
	exit
}

var selected = MENU.resume

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process_input(true)
	$CanvasLayer/Popup/resume.scale = Vector2(3, 3)
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _input(event):
	if Input.is_action_just_pressed("pause"):
		pause()
		
	if Input.is_action_just_pressed("ui_accept"):
		if get_node("../").get_tree().paused:
			select_menu_item(selected)
			
	if Input.is_action_pressed("ui_down"):
		$CanvasLayer/Popup.get_child(selected).scale = Vector2(2, 2)
		selected = selected + 1
		if selected == MENU.size():
			selected = 0
		$CanvasLayer/Popup.get_child(selected).scale = Vector2(3, 3)
		
	if Input.is_action_pressed("ui_up"):
		$CanvasLayer/Popup.get_child(selected).scale = Vector2(2, 2)
		selected = selected - 1
		if selected == -1:
			selected = MENU.size() - 1
		$CanvasLayer/Popup.get_child(selected).scale = Vector2(3, 3)

func pause():
	if get_node("../").get_tree().paused == false:
		get_node("../").get_tree().paused = true
		$CanvasLayer/Popup.show()
	else:
		get_node("../").get_tree().paused = false
		$CanvasLayer/Popup.hide()
		
func select_menu_item(selected):
	if selected == MENU.resume:
		pause()
	if selected == MENU.exit:
		get_node("../").get_tree().quit()
		
func save_game():
	var savegame = File.new()
	savegame.open("user://savegame.save", File.WRITE)
	