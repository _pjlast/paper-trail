extends KinematicBody2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
onready var animation_player = get_node("AnimationPlayer")
var velocity = Vector2(0, 0)
var input_matrix = [0, 0]
var last_direction = 1

var player_ball_scene = preload("res://prefabs/player_ball.tscn")
var player_ball = null

var is_blowing = false
var is_blow_stop = false

var is_on_floor = false

var blow_angle = 0

var blow_velocity = Vector2(0, 0)

const GRAVITY = 300

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	set_process_input(true)
	set_physics_process(true)
	pass
	
func _input(event):
	if Input.is_action_pressed("ui_right"):
		input_matrix[0] = 1
		play_animation("run")
		get_node("Sprite").flip_h = false
	if not Input.is_action_pressed("ui_right") and not Input.is_action_pressed("ui_left"):
		input_matrix[0] = 0
		play_animation("idle")
	if Input.is_action_pressed("ui_left"):
		input_matrix[0] = -1
		play_animation("run")
		get_node("Sprite").flip_h = true
		
	if Input.is_action_just_pressed("ball"):
		play_animation("fold")
		visible = false
		get_node("rectangle_collision_shape").disabled = true
		if player_ball == null:
			player_ball = player_ball_scene.instance()
			player_ball.get_node("Sprite").flip_h = get_node("Sprite").flip_h
			player_ball.set_position(position)
			player_ball.set_axis_velocity(velocity)
			player_ball.angular_velocity = input_matrix[0] * 5
			get_node("../").add_child(player_ball)
			
	if Input.is_action_just_released("ball"):
		if player_ball != null:
			play_animation("unfold")
			player_ball.get_node("CollisionPolygon2D").disabled = true
			player_ball.queue_free()
			velocity = player_ball.linear_velocity
			player_ball = null
			visible = true
			get_node("rectangle_collision_shape").disabled = false
		
	if Input.is_action_just_pressed("jump"):
		if player_ball == null and is_on_floor():
			velocity.y = -150


func _physics_process(delta):
	velocity.x = input_matrix[0] * 75
	
	if velocity.x != 0 and animation_player.current_animation == "idle":
		animation_player.current_animation = "run"
	
	if animation_player.current_animation != "unfold":
		if velocity.y < 0:
			play_animation("up_move")
		elif velocity.y > 0:
			if animation_player.current_animation == "up_move":
				animation_player.animation_set_next("jump_move_transition", "down_move")
				play_animation("jump_move_transition")
			elif animation_player.current_animation != "jump_move_transition" and animation_player.current_animation != "down_move":
				play_animation("down_move")
		else:
			if animation_player.current_animation == "down_move" or animation_player.current_animation == "down_still":
				play_animation("idle")
	
	velocity.y += GRAVITY * delta
	
	if visible == false:
		velocity.y = 0
	
	if is_blowing or is_blow_stop:
		velocity = blow_velocity
		if (is_blow_stop):
			velocity.x += input_matrix[0] * 75
		#get_node("Sprite").flip_h = true
	
	var remaining_velocity = move_and_slide(velocity)
	
	if is_on_floor() and velocity.y > 0:
		velocity.y = 0
		
	if player_ball != null:
		set_position(player_ball.position)
		
	if is_blowing:
		get_node("Sprite").flip_h = false
		rotation_degrees = blow_angle
		play_animation("blow")
	elif is_blow_stop:
		blow_velocity -= Vector2(cos(blow_angle/180*PI)*750*delta, sin(blow_angle/180*PI)*750*delta) - Vector2(0, GRAVITY * delta)
	else:
		rotation_degrees = 0
		blow_velocity = Vector2(0,0)

func _process(delta):
	pass
	
func play_animation(animation):
	if not is_blowing and not is_blow_stop:
		if animation_player.current_animation != "fold" and animation_player.current_animation != "unfold":
			animation_player.current_animation = animation
		else:
			animation_player.animation_set_next(animation_player.current_animation, animation)
	elif is_blowing:
		animation_player.current_animation = "blow"
	else:
		if animation == "blow":
			animation_player.current_animation = "blow"
		else:
			animation_player.current_animation = "blow_stop"
	
func blow(angle):
	play_animation("blow")
	blow_angle = angle
	blow_velocity = Vector2(cos(blow_angle/180*PI)*300, sin(blow_angle/180*PI)*300)
	is_blow_stop = false
	is_blowing = true
	pass
	
func stop():
	is_blowing = false
	is_blow_stop = true
	play_animation("blow_stop")
	pass
	
func blow_done():
	is_blow_stop = false
	
func is_on_floor():
	return get_node("ray_cast_1").is_colliding() or get_node("ray_cast_2").is_colliding() or get_node("ray_cast_3").is_colliding()
