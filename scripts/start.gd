extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var current_scene
var current_scene_path
var next_scene
var right_scene_path
var prev_scene
var left_scene_path
var next_right_scene_path
var next_left_scene_path

var left_scene
var right_scene
var top_scene
var bottom_scene

var previous_right_scene
var next_right_scene
var next_right_scene_position

var previous_left_scene
var next_left_scene
var next_left_scene_position

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	
	if global.resume_game:
		load_game()
	else:
		var first_level = load("res://scenes/test.tscn")
		current_scene_path = "res://scenes/test.tscn" 
		current_scene = first_level.instance()
		add_child(current_scene)
		
		right_scene_path = "res://scenes/test2.tscn"
		right_scene = load("res://scenes/test2.tscn").instance()
		right_scene.position.x = 384
		right_scene.get_node("Area2D").monitoring = false
		add_child(right_scene)
		right_scene.deactivate_transitions()
		current_scene.activate_transitions()
		current_scene.get_node("camera").make_current()


#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func transition_right_begin():
	current_scene.deactivate_transitions()
	if left_scene != null:
		previous_left_scene = left_scene
		
	left_scene = current_scene
	left_scene_path = current_scene_path
	
	current_scene = right_scene
	current_scene_path = right_scene_path
	if next_right_scene == null:
		right_scene = null
		right_scene_path = null
	else:
		right_scene = next_right_scene.instance()
		right_scene_path = next_right_scene_path
		right_scene.position = next_right_scene_position
		add_child(right_scene)
		right_scene.deactivate_transitions()
		next_right_scene = null
		next_right_scene_path = null
	
func transition_right():
	if previous_left_scene != null:
		previous_left_scene.queue_free()
		previous_left_scene = null
	
	current_scene.activate_transitions()
	current_scene.get_node("camera").position.x = 0
	current_scene.get_node("camera").make_current()
	save_game()
	
func transition_left_begin():
	current_scene.deactivate_transitions()
	if right_scene != null:
		previous_right_scene = right_scene
		
	right_scene = current_scene
	right_scene_path = current_scene_path
	
	current_scene = left_scene
	current_scene_path = left_scene_path
	
	if next_left_scene == null:
		left_scene = null
		left_scene_path = null
	else:
		left_scene = next_left_scene.instance()
		left_scene_path = next_left_scene_path
		left_scene.position = next_left_scene_position
		add_child(left_scene)
		left_scene.deactivate_transitions()
		next_left_scene = null
		next_left_scene_path = null
	
	
func transition_left():
	if previous_right_scene != null:
		previous_right_scene.queue_free()
		previous_right_scene = null
		
	current_scene.activate_transitions()
	current_scene.get_node("camera").position.x = 0
	current_scene.get_node("camera").make_current()
	save_game()
	
func transition_up():
	pass
	
func transition_down():
	pass
	
func save():
	var savedict = {}
	if left_scene and right_scene:
		savedict = {
			player_posx=$player.position.x,
			player_posy=$player.position.y,
			current_scene_path=current_scene_path,
			current_scene_posx=current_scene.position.x,
			current_scene_posy=current_scene.position.y,
			left_scene_path=left_scene_path,
			left_scene_posx=left_scene.position.x,
			left_scene_posy=left_scene.position.y,
			right_scene_path=right_scene_path,
			right_scene_posx=right_scene.position.x,
			right_scene_posy=right_scene.position.y,
		}
	elif left_scene:
		savedict = {
			player_posx=$player.position.x,
			player_posy=$player.position.y,
			current_scene_path=current_scene_path,
			current_scene_posx=current_scene.position.x,
			current_scene_posy=current_scene.position.y,
			left_scene_path=left_scene_path,
			left_scene_posx=left_scene.position.x,
			left_scene_posy=left_scene.position.y,
			right_scene_path=null,
			right_scene_posx=null,
			right_scene_posy=null,
		}
	elif right_scene:
		savedict = {
			player_posx=$player.position.x,
			player_posy=$player.position.y,
			current_scene_path=current_scene_path,
			current_scene_posx=current_scene.position.x,
			current_scene_posy=current_scene.position.y,
			left_scene_path=null,
			left_scene_posx=null,
			left_scene_posy=null,
			right_scene_path=right_scene_path,
			right_scene_posx=right_scene.position.x,
			right_scene_posy=right_scene.position.y,
		}
	
	return savedict
	
func save_game():
	var savegame = File.new()
	savegame.open("user://savegame.save", File.WRITE)
	savegame.store_line(to_json(save()))
	savegame.close()
	
func load_game():
	var savegame = File.new()
	if !savegame.file_exists("user://savegame.save"):
		return
		
	var currentline = {}
	savegame.open("user://savegame.save", File.READ)
	currentline = parse_json(savegame.get_line())
	
	current_scene_path = currentline["current_scene_path"]
	current_scene = load(current_scene_path).instance()
	current_scene.position = Vector2(currentline["current_scene_posx"], currentline["current_scene_posy"])
	add_child(current_scene)
	
	right_scene_path = currentline["right_scene_path"]
	
	if right_scene_path:
		right_scene = load(right_scene_path).instance()
		right_scene.position = Vector2(currentline["right_scene_posx"], currentline["right_scene_posy"])
		add_child(right_scene)
	
	left_scene_path = currentline["left_scene_path"]
	if left_scene_path:
		left_scene = load(left_scene_path).instance()
		left_scene.position = Vector2(currentline["left_scene_posx"], currentline["left_scene_posy"])
		add_child(left_scene)
	
	$player.position = Vector2(currentline["player_posx"], currentline["player_posy"])
	
	if right_scene:
		right_scene.deactivate_transitions()
	if left_scene:
		left_scene.deactivate_transitions()
	current_scene.activate_transitions()
	current_scene.get_node("camera").make_current()
	
	savegame.close()
	