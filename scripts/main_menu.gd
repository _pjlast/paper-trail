extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

enum MENU{
	resume_game,
	new_game,
	exit
}

var selected = MENU.resume_game

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	set_process_input(true)
	$CanvasLayer/Popup.get_child(selected).scale = Vector2(3, 3)
	$CanvasLayer/Popup.show()
	
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _input(event):
		
	if Input.is_action_just_pressed("ui_accept"):
		select_menu_item(selected)
			
	if Input.is_action_pressed("ui_down"):
		$CanvasLayer/Popup.get_child(selected).scale = Vector2(2, 2)
		selected = selected + 1
		if selected == MENU.size():
			selected = 0
		$CanvasLayer/Popup.get_child(selected).scale = Vector2(3, 3)
		
	if Input.is_action_pressed("ui_up"):
		$CanvasLayer/Popup.get_child(selected).scale = Vector2(2, 2)
		selected = selected - 1
		if selected == -1:
			selected = MENU.size() - 1
		$CanvasLayer/Popup.get_child(selected).scale = Vector2(3, 3)
		
func select_menu_item(selected):
	if selected == MENU.resume_game:
		global.resume_game = true
		get_tree().change_scene("res://scenes/start.tscn")
	if selected == MENU.new_game:
		global.resume_game = false
		get_tree().change_scene("res://scenes/start.tscn")
	if selected == MENU.exit:
		get_tree().quit()