extends Node

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	if global.level_position == 1:
		get_node("player").position = get_node("Node2D").position
		get_node("player").position.y = global.player_position.y
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func deactivate_transitions():
	get_node("Area2D").monitoring = false
	
func activate_transitions():
	get_node("Area2D").monitoring = true